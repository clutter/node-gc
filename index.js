"use strict";

const EventEmitter = require("events");
const sma = require("sma");
const trend = require("trend");

const nodeGc = new EventEmitter();
var handle = null;
var stats = [];

if (!global.gc) {
    throw new Error("run node with --expose-gc");
}

function forceGC() {
    let arr;
    let data = {
        "min": {},
        "max": {},
        "avg": {},
        "now": {}
    };

    global.gc();
    stats.push(process.memoryUsage());
    if (stats.length > nodeGc.samples) {
        stats.splice(0, stats.length - nodeGc.samples); // eslint-disable-line no-magic-numbers
    }
    // get min, max, avg
    ["rss", "heapTotal", "heapUsed", "external"].forEach(function eachKey(key) {
        let val;

        arr = stats.map(a => a[key]);
        val = Math.min.apply(null, arr);
        data.min[key] = !data.min[key] || (val < data.min[key]) ? val : data.min[key];
        val = Math.max.apply(null, arr);
        data.max[key] = !data.max[key] || (val > data.max[key]) ? val : data.max[key];
        data.avg[key] = arr.reduce(function reduce(a, b) {
            return a + b;
        }) / arr.length;
        data.now[key] = stats[stats.length - 1][key]; // eslint-disable-line no-magic-numbers
    });
    nodeGc.emit("stats", data);
    arr = stats.map(a => a.rss);
    if (arr.length < nodeGc.window) {
        return;
    }
    arr = sma(arr, nodeGc.window, function convert(n) {
        return n;
    });
    if (arr.length < nodeGc.window) {
        return;
    }
    nodeGc.emit("sma-trend", trend(arr, {
        "lastPoints": 1,
        "avgPoints": arr.length - 1, // eslint-disable-line no-magic-numbers
        "reversed": false
    }));
}

Object.defineProperty(nodeGc, "interval", {
    "set": function set(ival) {
        this._interval = Number(ival) || this._interval;
    },
    "get": function get() {
        return this._interval;
    },
    "configurable": false,
    "enumerable": true
});
nodeGc.interval = 600000; // default: 10-min

Object.defineProperty(nodeGc, "samples", {
    "set": function set(val) {
        this._samples = Number(val) || this._samples;
    },
    "get": function get() {
        return this._samples;
    },
    "configurable": false,
    "enumerable": true
});
nodeGc.samples = 9; // default: 9-samples

Object.defineProperty(nodeGc, "window", {
    "set": function set(val) {
        this._window = Number(val) || this._window;
    },
    "get": function get() {
        return this._window;
    },
    "configurable": false,
    "enumerable": true
});
nodeGc.window = 3; // default: 3-items for sma

Object.defineProperty(nodeGc, "start", {
    "value": function value() {
        if (!handle) {
            handle = setInterval(forceGC, this._interval).unref();
        }
    },
    "writable": false,
    "configurable": false,
    "enumerable": true
});

Object.defineProperty(nodeGc, "stop", {
    "value": function value() {
        if (handle) {
            clearInterval(handle);
            handle = null;
        }
    },
    "writable": false,
    "configurable": false,
    "enumerable": true
});

module.exports = nodeGc;
