/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint max-nested-callbacks: ["error", 5]*/
/*eslint no-magic-numbers: "off"*/

"use strict";

const assert = require("assert");
const EventEmitter = require("events");
const nodeGc = require("../");

before(function (done) {
    done();
});

describe("ConstConfig", function () {
    it("should be an instanceof EventEmitter", function (done) {
        assert.ok(nodeGc instanceof EventEmitter);
        done();
    });
    it("should have default properties", function (done) {
        assert.strictEqual(nodeGc.interval, 600000);
        assert.strictEqual(nodeGc.samples, 9);
        assert.strictEqual(nodeGc.window, 3);
        done();
    });
    it("should be able to override defaults", function (done) {
        nodeGc.interval = 100;
        nodeGc.samples = 6;
        nodeGc.window = 2;
        assert.strictEqual(nodeGc.interval, 100);
        assert.strictEqual(nodeGc.samples, 6);
        assert.strictEqual(nodeGc.window, 2);
        done();
    });
    it("should start and emit stats event", function (done) {
        nodeGc.once("stats", function (data) {
            ["min", "max", "avg", "now"].forEach(function (type) {
                assert.ok(data[type]);
                ["rss", "heapTotal", "heapUsed", "external"].forEach(function (key) {
                    assert.ok(data[type][key]);
                });
            });
            nodeGc.stop();
            done();
        });
        nodeGc.start();
    });
    it("should start and emit sma-trend event", function (done) {
        this.timeout(5000); // eslint-disable-line no-invalid-this
        nodeGc.on("sma-trend", function (trend) {
            assert.ok(Number.isFinite(trend) && parseFloat(trend) === trend);
            nodeGc.stop();
            done();
        });
        nodeGc.start();
    });
});

after(function (done) {
    done();
});
