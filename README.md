# NodeJS Garbage-collection
Regular garbage-collection with stats and trend based on simple-moving-averages

## Installation

```bash
$ npm install @parthar/node-gc --save
```

## Usage

```js
// launch NodeJS with --expose-gc
// else, throws Error
var nodeGc = require("@parthar/node-gc");

nodeGc.interval = 10 * 60 * 1000; // GC interval; default: 10-min
nodeGC.samples = 9; // number of samples to store for calculating simple-moving-averages
nodeGC.window = 3; // size of simple-moving-average window; default: 3

// start GC
nodeGC.start();

// stop GC
nodeGC.stop()

// stats event
nodeGC.on('stats', function (stats) {
    // each of these has details of: rss, heapTotal, heapUsed, external
    // stats.min => minimum ever values so far
    // stats.max => maximum ever values so far
    // stats.avg => average values for available sample-size
    // stats.now => current values
});

// simple-moving-averages trend event (needs twice window-size samples)
// compares the last item from simple-moving-averages to previous items for trending
nodeGC.on('sma-trend', function (trend) {
    // >1 means growth in rss-usage metric
    // <1 means reduction in rss-usage metric
});
```
